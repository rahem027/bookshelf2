﻿using DataAccessLibrary;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Bookshelf
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class BookInfo_View : Page
    {
        private Book OriginalBook = new Book();                         

        public BookInfo_View()
        {
            this.InitializeComponent();

            /**
             * When the page is loaded, to highlight the selected items on the list view of tags, this method need to be called. 
             * This cannot be done in OnNavigatedTo() method because the list view may not be exist when that method is called.            
             */

            this.Loaded += OnPageLoaded;
            this.Unloaded += OnPageUnloaded;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            OriginalBook = (Book)e.Parameter;
 
            foreach(string tag in DataAccess_Tags.GetTags())
            {
                TagsListView.Items.Add(tag);
            }

            TitleTextBox.Text = OriginalBook.Title;
            AuthorTextBox.Text = OriginalBook.Author;
            PublisherTextBox.Text = OriginalBook.Publisher;
            ISBNTextBox.Text = OriginalBook.ISBN;
            QuantityTextBox.Text = OriginalBook.Quantity.ToString();
        }

        private void OnPageLoaded(object sender, RoutedEventArgs e)
        {
            string[] selectedTags = OriginalBook.Tags.Split(';', System.StringSplitOptions.RemoveEmptyEntries);

            foreach(string selectedTag in selectedTags)
            {
                TagsListView.SelectedItems.Add(selectedTag);
            }            
        }

        private async void OnPageUnloaded(object sender, RoutedEventArgs e)
        {
            Book ModifiedBook = new Book();

            //make sure that the modifications are correct. For example Title cannot be left empty
            string title = TitleTextBox.Text,
                   author = AuthorTextBox.Text,
                   publisher = PublisherTextBox.Text,
                   isbn = ISBNTextBox.Text,
                   tags = "";

            if (TagsListView.SelectedItems.Count > 0)
                tags = string.Join(';', TagsListView.SelectedItems);

            int? quantity = null;

            if (!String.IsNullOrEmpty(QuantityTextBox.Text))
                quantity = Convert.ToInt32(QuantityTextBox.Text);

            //make sure the input is correct
            if (String.IsNullOrEmpty(title))
            {
                ContentDialog cd = new ContentDialog
                {
                    Title = "Oops!",
                    Content = "Title may be missing!",
                    CloseButtonText = "Ok"
                };

                await cd.ShowAsync();
            }

            else if (String.IsNullOrEmpty(author))
            {
                ContentDialog cd = new ContentDialog
                {
                    Title = "Oops!",
                    Content = "Author may be missing!",
                    CloseButtonText = "Ok"
                };

                await cd.ShowAsync();
            }

            else if (String.IsNullOrEmpty(publisher))
            {
                ContentDialog cd = new ContentDialog
                {
                    Title = "Oops!",
                    Content = "Publisher may be missing!",
                    CloseButtonText = "Ok"
                };

                await cd.ShowAsync();
            }

            else if (String.IsNullOrEmpty(isbn))
            {
                ContentDialog cd = new ContentDialog
                {
                    Title = "Oops!",
                    Content = "Title may be missing!",
                    CloseButtonText = "Ok"
                };

                await cd.ShowAsync();
            }

            else if (!quantity.HasValue)
            {
                ContentDialog cd = new ContentDialog
                {
                    Title = "Oops!",
                    Content = "Quantity may be missing!",
                    CloseButtonText = "Ok"
                };

                await cd.ShowAsync();
            }
            else if (tags == "")
            {
                ContentDialog cd = new ContentDialog
                {
                    Title = "Oops!",
                    Content = "Please select at least one tag",
                    CloseButtonText = "Ok"
                };

                await cd.ShowAsync();
            }
            else
            {
                ModifiedBook.Title = title;
                ModifiedBook.Author = author;
                ModifiedBook.Publisher = publisher;
                ModifiedBook.ISBN = isbn;
                ModifiedBook.Quantity = quantity.GetValueOrDefault();
                ModifiedBook.Tags = tags;
                ModifiedBook.CoverImageLocation = OriginalBook.CoverImageLocation;
            }
             
            DataAccess_Books.UpdateBook(OriginalBook, ModifiedBook);
        }
    }
}
