﻿using System.Collections.Generic;
using Windows.Web.Http;
using System;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using System.Runtime.Serialization;

namespace Bookshelf.Data
{
    public class BookInfoProxy
    {
        public static async Task<RootObject> GetBooks(string title)
        {
            var http = new HttpClient();

            string url = "https://www.googleapis.com/books/v1/volumes?q=" + title.Replace(' ', '+') + "&fields=items(volumeInfo(title, authors, publisher, imageLinks/smallThumbnail))";
            var response = await http.GetAsync(new Uri(url));
            var result = await response.Content.ReadAsStringAsync();

            var serializer = new DataContractJsonSerializer(typeof(RootObject));

            var ms = new MemoryStream(Encoding.UTF8.GetBytes(result));
            var data = (RootObject)serializer.ReadObject(ms);

            return data;
        }
    }

    [DataContract]
    public class ImageLinks
    {
        [DataMember]
        public string smallThumbnail { get; set; }

        [DataMember]
        public string thumbnail { get; set; }
    }

    [DataContract]
    public class VolumeInfo
    {
        [DataMember]
        public string title { get; set; }

        [DataMember]
        public List<string> authors { get; set; }

        [DataMember]
        public ImageLinks imageLinks { get; set; }

        [DataMember]
        public string publisher { get; set; }
    }

    [DataContract]
    public class Item
    {
        [DataMember]
        public VolumeInfo volumeInfo { get; set; }
    }

    [DataContract]
    public class RootObject
    {
        [DataMember]
        public List<Item> items { get; set; }
    }
}
