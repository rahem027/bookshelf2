﻿using DataAccessLibrary;
using System.Collections.ObjectModel;

namespace Bookshelf
{
    public class BooksViewModel
    {
        private ObservableCollection<Book> books { get; set; }

        public ObservableCollection<Book> Books
        {
            get { return this.books; }

            set
            {
                this.books = value;
            }
        }

        public BooksViewModel()
        {
            books = DataAccess_Books.GetBooks(BookInfoType.Minimal);
        }
    }
}
