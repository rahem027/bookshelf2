﻿using Bookshelf.Data;
using System.Net.NetworkInformation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using System;
using Windows.Storage;
using Windows.Web.Http;
using DataAccessLibrary;
using System.Linq;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Bookshelf
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class NewBook_View : Page
    {
        private string CoverImageUri = "ms-appx:///Assets/LockScreenLogo.scale-200.png";

        public NewBook_View()
        {
            this.InitializeComponent();

            foreach (string tag in DataAccess_Tags.GetTags())
                TagsListView.Items.Add(tag);

            CoverImage.Source = new BitmapImage(new Uri("ms-appx:///Assets/LockScreenLogo.scale-200.png"));
        }

        private void This_is_not_the_book(object sender, RoutedEventArgs e)
        {
            //the book found is not what user wants. Use the default image
            CoverImage.Source = new BitmapImage(new Uri("ms-appx:///Assets/LockScreenLogo.scale-200.png"));
            CoverImageUri = "ms-appx:///Assets/LockScreenLogo.scale-200.png";
        }

        private async void AddBook(object sender, RoutedEventArgs e)
        {
            string title = TitleTextBox.Text,
                   author = AuthorTextBox.Text,
                   publisher = PublisherTextBox.Text,
                   isbn = ISBNTextBox.Text,
                   tags = string.Join(';', TagsListView.SelectedItems.ToList().Cast<string>());

            int? quantity = null;

            if(!String.IsNullOrEmpty(QuantityTextBox.Text))
                quantity = Convert.ToInt32(QuantityTextBox.Text);

            //make sure the input is correct
            if (String.IsNullOrEmpty(title))
            {
                ContentDialog cd = new ContentDialog
                {
                    Title = "Oops!",
                    Content = "Title may be missing!",
                    CloseButtonText = "Ok"
                };

                await cd.ShowAsync();
            }

            else if (String.IsNullOrEmpty(author))
            {
                ContentDialog cd = new ContentDialog
                {
                    Title = "Oops!",
                    Content = "Author may be missing!",
                    CloseButtonText = "Ok"
                };

                await cd.ShowAsync();
            }

            else if (String.IsNullOrEmpty(publisher))
            {
                ContentDialog cd = new ContentDialog
                {
                    Title = "Oops!",
                    Content = "Publisher may be missing!",
                    CloseButtonText = "Ok"
                };

                await cd.ShowAsync();
            }

            else if (String.IsNullOrEmpty(isbn))
            {
                ContentDialog cd = new ContentDialog
                {
                    Title = "Oops!",
                    Content = "Title may be missing!",
                    CloseButtonText = "Ok"
                };

                await cd.ShowAsync();
            }

            else if (!quantity.HasValue)
            {
                ContentDialog cd = new ContentDialog
                {
                    Title = "Oops!",
                    Content = "Quantity may be missing!",
                    CloseButtonText = "Ok"
                };

                await cd.ShowAsync();
            }
            else if(tags.Count() == 0)
            {
                ContentDialog cd = new ContentDialog
                {
                    Title = "Oops!",
                    Content = "Please select at least one tag",
                    CloseButtonText = "Ok"
                };

                await cd.ShowAsync();
            }
            else
            {
                //all the input is correct
                if (CoverImageUri != "ms-appx:///Assets/LockScreenLogo.scale-200.png")
                {
                    Uri uri = new Uri(CoverImageUri);

                    const string coverImageFolder = "Cover Image Location";

                    //remove the special characters from the picture file's name so that windows does not have a problem
                    string tempTitle = new string((from c in title
                                                   where char.IsWhiteSpace(c) || char.IsLetterOrDigit(c) || c == '_'
                                                   select c).ToArray());                         

                    string fileName = tempTitle + ".jpg";

                    StorageFolder folder = await ApplicationData.Current.LocalFolder.CreateFolderAsync(coverImageFolder, CreationCollisionOption.OpenIfExists);
                    StorageFile file = await folder.CreateFileAsync(fileName, CreationCollisionOption.GenerateUniqueName);

                    HttpClient client = new HttpClient();
                    var buffer = await client.GetBufferAsync(uri);
                    await Windows.Storage.FileIO.WriteBufferAsync(file, buffer);

                    CoverImageUri = file.Path;
                }

                Book b = new Book
                {
                    Title = title,
                    Author = author,
                    Publisher = publisher,
                    ISBN = isbn,
                    Quantity = quantity.GetValueOrDefault(),
                    CoverImageLocation = CoverImageUri,
                    Tags = tags
                };

                DataAccess_Books.AddBook(b);
                Frame.Navigate(typeof(YourBooks_View));
            }
        }

        private async void TitleTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            string title = TitleTextBox.Text;

            if (!String.IsNullOrEmpty(title))
            {
                bool isInternetAvailable = NetworkInterface.GetIsNetworkAvailable();

                //if internet is available, then use google books to find whatever information it can
                if (isInternetAvailable)
                {
                    //make sure the user knows that some processing is going on
                    myProgressRing.IsActive = true;

                    RootObject bookInfo = await BookInfoProxy.GetBooks(title);
                    
                    //look for the first book whose title matches the title of the book entered by the user irrespective of case
                    foreach(Item i in bookInfo.items)
                    {
                        if(string.Equals(i.volumeInfo.title, title, StringComparison.OrdinalIgnoreCase))
                        {
                            if(i.volumeInfo.authors?.Count > 0)
                                AuthorTextBox.Text = string.Join(", ", i.volumeInfo.authors);

                            if (!String.IsNullOrEmpty(i.volumeInfo.publisher))
                                PublisherTextBox.Text = i.volumeInfo.publisher;

                            if (!String.IsNullOrEmpty(i.volumeInfo.imageLinks.smallThumbnail))
                            {
                                CoverImage.Source = new BitmapImage(new Uri(i.volumeInfo.imageLinks.smallThumbnail, UriKind.Absolute));
                                CoverImageUri = i.volumeInfo.imageLinks.smallThumbnail;
                            }

                            break;
                        }
                    }

                    myProgressRing.IsActive = false;
                }
            }            
        }


    }
}
