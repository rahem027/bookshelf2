﻿using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Bookshelf
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            myFrame.Navigate(typeof(YourBooks_View));

            ApplicationView.GetForCurrentView().Consolidated += (ss, ee) => { Application.Current.Exit(); };
        }

        private void NavView_BackRequested(NavigationView sender, NavigationViewBackRequestedEventArgs args)
        {
            if (myFrame.CanGoBack)
                myFrame.GoBack();
        }

        private void NavView_SelectionChanged(NavigationView sender, NavigationViewSelectionChangedEventArgs args)
        {
            if (YourBooks_NavItem.IsSelected)
                myFrame.Navigate(typeof(YourBooks_View));

            else if (RecentActivities_NavItem.IsSelected)
                myFrame.Navigate(typeof(RecentActivities_View));
        }
    }
}
