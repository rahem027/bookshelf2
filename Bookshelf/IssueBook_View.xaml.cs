﻿using DataAccessLibrary;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using System;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Bookshelf
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>

    public delegate void ReduceQuantity(string title);
    
    public sealed partial class IssueBook_View : Page
    {
        public static event ReduceQuantity Event_ReduceQuantity;

        Log log;

        public IssueBook_View()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            log = (Log)e.Parameter;

            if (string.IsNullOrEmpty(log.Title))
                throw new Exception("Title not found");
        }

        private async void Ok_Click(object sender, RoutedEventArgs e)
        {
            string Borrower = BorrowerTextBox.Text,
                   Class = ClassTextBox.Text,
                   Division = DivisionTextBox.Text;

            if(string.IsNullOrEmpty(Borrower))
            {
                ContentDialog cd = new ContentDialog
                {
                    Title = "Oops!",
                    Content = "Borrower name might be missing",                    
                    CloseButtonText = "Ok"
                };

                await cd.ShowAsync();
            }

            else if(string.IsNullOrEmpty(Class))
            {
                ContentDialog cd = new ContentDialog
                {
                    Title = "Oops!",
                    Content = "Class might be missing",
                    CloseButtonText = "Ok"
                };

                await cd.ShowAsync();
            }

            else if(string.IsNullOrEmpty(Division))
            {
                ContentDialog cd = new ContentDialog
                {
                    Title = "Oops!",
                    Content = "Division might be missing",
                    CloseButtonText = "Ok"
                };

                await cd.ShowAsync();
            }

            else
            {
                log.Borrower = Borrower;
                log.Class = Class;
                log.Division = Division;
                log.DateBorrowed = DateTime.Today;

                
                DataAccess_Log.IssueBook(log);
                Event_ReduceQuantity.Invoke(log.Title);

                Frame.GoBack();               
            }
        }
    }
}
