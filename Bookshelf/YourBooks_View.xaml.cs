﻿using DataAccessLibrary;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.ApplicationModel.DataTransfer;
using System.Collections.Generic;
using System.Linq;
using System;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Bookshelf
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class YourBooks_View : Page
    {        
        private BooksViewModel ViewModel { get; set; } = new BooksViewModel();

        List<Book> DragDropBuffer;
        
        public YourBooks_View()
        {
            this.InitializeComponent();

            //subscirbe to the events by DataAccess
            DataAccess_Books.Event_AddBook += new AddBook_Delegate(this.GridView_AddBook);
            DataAccess_Books.Event_UpdateBook += new UpdateBook_Delegate(this.GridView_UpdateBook);
            IssueBook_View.Event_ReduceQuantity += new ReduceQuantity(this.GridView_ReduceQuantity);
        }

        private void NewBook_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(NewBook_View));
        }

        private async void DeleteBook_Click(object sender, RoutedEventArgs e)
        {
            var books = AllBooks_GridView.SelectedItems.ToList();  
            
            if(books.Count < 1)
            {
                ContentDialog cd = new ContentDialog
                {
                    Title = "Oops!",
                    Content = "Please select a book to delete before pressing the button",
                    CloseButtonText = "Ok"
                };

                await cd.ShowAsync();
            }

            foreach (var b in books)
            {
                var book = b as Book;

                DataAccess_Books.DeleteBook(book.Title);
                ViewModel.Books.Remove(book);
            }
        }

        private void DeleteButton_DragOver(object sender, DragEventArgs e)
        {
            e.AcceptedOperation = DataPackageOperation.Move;
            e.DragUIOverride.Caption = "Delete Book(s)";
        }

        private void DeleteButton_OnDrop(object sender, DragEventArgs e)
        {
            foreach (Book b in DragDropBuffer)
            {
                ViewModel.Books.Remove(b);
                DataAccess_Books.DeleteBook(b.Title);
            }
        }

        private void GridView_AddBook(Book book)
        {
            ViewModel.Books.Add(book); 
        }

        public void GridView_UpdateBook(Book a, Book b)
        {
            int index = -1;

            foreach(Book book in ViewModel.Books)
            {
                ++index;

                if (book.Equals(a))
                    break;
            }

            ViewModel.Books.Insert(index, b);
            ViewModel.Books.RemoveAt(index + 1);
        }        

        private void AllBooks_GridView_DragItemsStarting(object sender, DragItemsStartingEventArgs e)
        {
            DragDropBuffer = new List<Book>(e.Items.Cast<Book>());
        }        

        private async void Edit_Click(object sender, RoutedEventArgs e)
        {
            Book selectedBook = (Book)AllBooks_GridView.SelectedItem;

            if (selectedBook == null)
            {
                ContentDialog cd = new ContentDialog
                {
                    Title = "Oops!",
                    Content = "You might want to select a book to edit before clicking the button",
                    CloseButtonText = "Ok"
                };

                await cd.ShowAsync();
            }

            //query the info about the selected book from the database and store it in selectedBook
            selectedBook = DataAccess_Books.GetInfoAboutBook(selectedBook.Title);
            Frame.Navigate(typeof(BookInfo_View), selectedBook);
        }

        private void Edit_DragOver(object sender, DragEventArgs e)
        {
            e.AcceptedOperation = DataPackageOperation.Copy;
            e.DragUIOverride.Caption = "Edit Book";
        }

        private async void Edit_OnDrop(object sender, DragEventArgs e)
        {
            if(DragDropBuffer.Count > 1)
            {
                ContentDialog cd = new ContentDialog
                {
                    Title = "Oops!",
                    Content = "Only one book can be edited at a time",
                    CloseButtonText = "Ok"
                };

                await cd.ShowAsync();
            }

            Book DraggedBook = DragDropBuffer.ElementAt(0);

            DraggedBook = DataAccess_Books.GetInfoAboutBook(DraggedBook.Title);

            Frame.Navigate(typeof(BookInfo_View), DraggedBook);
        }

        private void IssueBook_Click(object sender, RoutedEventArgs e)
        {
            Book b = (Book)AllBooks_GridView.SelectedItem;
            Log l = new Log();

            l.Title = b.Title;

            Frame.Navigate(typeof(IssueBook_View), l);
        }

        private void IssueBook_DragOver(object sender, DragEventArgs e)
        {
            e.AcceptedOperation = DataPackageOperation.Move;
            e.DragUIOverride.Caption = "Issue Book";
        }

        private void IssueBookOnDrop(object sender, DragEventArgs e)
        {
            Log l = new Log();
            l.Title = DragDropBuffer[0].Title;
            Frame.Navigate(typeof(IssueBook_View), l);
        }

        private void GridView_ReduceQuantity(string title)
        {
            foreach(Book b in ViewModel.Books)
            {
                if(b.Title == title)
                {
                    Book book = DataAccess_Books.GetInfoAboutBook(b.Title);

                    //i need to do this to get a copy. Unfortunately, i dont know of any standard method that does this
                    Book temp = new Book
                    {
                        Title = book.Title,
                        Author = book.Author,
                        Publisher = book.Publisher,
                        Quantity = book.Quantity,
                        CoverImageLocation = book.CoverImageLocation,
                        Tags = book.Tags,
                        ISBN = book.ISBN
                    };
                    //dont need to intialize other memebers because the ViewModel.GetBooks() is called with Minimal 

                    --temp.Quantity;
                    
                    ViewModel.Books[ViewModel.Books.IndexOf(b)] = temp;
                    DataAccess_Books.UpdateBook(book, temp);
                    break;
                }
            }
        }           
    }
}
