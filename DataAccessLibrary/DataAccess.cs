﻿using Microsoft.Data.Sqlite;

namespace DataAccessLibrary
{
    public class Book
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public string Publisher { get; set; }
        public string ISBN { get; set; }

        public int Quantity { get; set; }

        public string CoverImageLocation { get; set; }

        public string Tags { get; set; }

        //not overriding equals because it needed giving a lot of unwanted implementations
        public bool Equals(Book b)
        {
            if (this.Title == b.Title && this.Author == b.Author && this.Publisher == b.Publisher && this.ISBN == b.ISBN &&
                this.Quantity == b.Quantity && this.CoverImageLocation == b.CoverImageLocation && this.Tags == b.Tags)
                return true;

            return false;
        }
    }

    public static class DataAccess
    {
        public static readonly string BooksFileName = "MyBooks.db";
        public static readonly string LogFileName = "Log.db";

        private static void InitializeMyBooks()
        {
            using (SqliteConnection db = new SqliteConnection("Filename= " + BooksFileName))
            {
                db.Open();

                //create MyBooks table if not exists
                string command = "create table if not exists MyBooks" +
                    "(Title text Primary Key, Author text not null, Publisher text not null, ISBN text not null, Quantity int not null, CoverImageLocation text not null, Tags text not null)";

                SqliteCommand createTable = new SqliteCommand(command, db);

                createTable.ExecuteReader();
            }
        }

        private static void InitializeTags()
        {
            using (SqliteConnection db = new SqliteConnection("Filename=" + BooksFileName))
            {
                db.Open();
                //check if tags table exists                           
                if (!CheckIfTableExists("Tags"))
                {
                    SqliteCommand createTagsTable = new SqliteCommand
                    {
                        CommandText = "create table Tags(Name text Primary Key)",
                        Connection = db
                    };

                    createTagsTable.ExecuteReader();

                    string[] tags = { "Fiction", "Romance", "Action", "Adventure", "Non-Fiction",
                                      "Reference books", "Comics", "Travel", "E - book" };

                    createTagsTable = new SqliteCommand
                    {
                        Connection = db,
                        CommandText = "insert into Tags values" +
                                      "('" + tags[0] + "'), " +
                                      "('" + tags[1] + "')," +
                                      "('" + tags[2] + "'), " +
                                      "('" + tags[3] + "'), " +
                                      "('" + tags[4] + "'), " +
                                      "('" + tags[5] + "'), " +
                                      "('" + tags[6] + "'), " +
                                      "('" + tags[7] + "'), " +
                                      "('" + tags[8] + "')"
                    };

                    createTagsTable.ExecuteReader();
                }
            }
        }

        private static void InitializeLogDb()
        {
            using (SqliteConnection db = new SqliteConnection("Filename=" + LogFileName))
            {
                db.Open();

                SqliteCommand command = new SqliteCommand
                {
                    CommandText = "create table if not exists Log(Title text not null, Borrower text not null, Class text not null, " +
                    "Division text not null, DateBorrowed text not null, DateReturned text)",

                    Connection = db
                };

                command.ExecuteReader();
            }
        }

        private static bool CheckIfTableExists(string tableName)
        {
            using (SqliteConnection db = new SqliteConnection("Filename=" + BooksFileName))
            {
                db.Open();

                SqliteCommand checkIfTableExists = new SqliteCommand
                {
                    CommandText = "select name from sqlite_master where type='table' and name=@TableName",
                    Connection = db
                };

                checkIfTableExists.Parameters.AddWithValue("@TableName", tableName);

                SqliteDataReader query = checkIfTableExists.ExecuteReader();

                bool TableExists = false;
                if (query.Read())
                {
                    TableExists = query.GetString(0) == "Tags" ? true : false;
                }

                return TableExists;
            }
        }

        

        public static void InitializeDatabase()
        {
            InitializeMyBooks();
            InitializeTags();
            InitializeLogDb();
        }               
    }
}
