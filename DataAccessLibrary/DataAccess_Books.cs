﻿using Microsoft.Data.Sqlite;
using System.Collections.ObjectModel;

namespace DataAccessLibrary
{
    public delegate void AddBook_Delegate(Book b);
    public delegate void UpdateBook_Delegate(Book a, Book b);

    public enum BookInfoType { Minimal, Full };

    public static class DataAccess_Books
    {
        public static event AddBook_Delegate Event_AddBook;
        public static event UpdateBook_Delegate Event_UpdateBook;

        public static void AddBook(Book book)
        {
            Event_AddBook.Invoke(book);

            using (SqliteConnection db = new SqliteConnection("Filename=" + DataAccess.BooksFileName))
            {
                db.Open();

                SqliteCommand insertCommand = new SqliteCommand
                {
                    Connection = db,
                    CommandText = "insert into MyBooks values(@Title, @Author, @Publisher, @ISBN, @Quantity, @CoverImageLocation, @Tags)"
                };

                insertCommand.Parameters.AddWithValue("@Title", book.Title);
                insertCommand.Parameters.AddWithValue("@Author", book.Author);
                insertCommand.Parameters.AddWithValue("@Publisher", book.Publisher);
                insertCommand.Parameters.AddWithValue("@ISBN", book.ISBN);
                insertCommand.Parameters.AddWithValue("@Quantity", book.Quantity);
                insertCommand.Parameters.AddWithValue("@CoverImageLocation", book.CoverImageLocation);
                insertCommand.Parameters.AddWithValue("@Tags", book.Tags);

                insertCommand.ExecuteReader();

                db.Close();
            }
        }

        public static void DeleteBook(string title)
        {
            using (SqliteConnection db = new SqliteConnection("Filename=" + DataAccess.BooksFileName))
            {
                db.Open();

                SqliteCommand command = new SqliteCommand
                {
                    CommandText = "delete from MyBooks where Title=@title",
                    Connection = db
                };

                command.Parameters.AddWithValue("@title", title);

                command.ExecuteReader();
            }
        }

        public static void UpdateBook(Book OldBook, Book NewBook)
        {
            if (OldBook.Equals(NewBook))
                return;

            using (SqliteConnection db = new SqliteConnection("Filename=" + DataAccess.BooksFileName))
            {
                db.Open();

                SqliteCommand command = new SqliteCommand
                {
                    CommandText = "update MyBooks set " +
                                  "Title = @Title, Author = @Author, Publisher = @Publisher, ISBN = @ISBN, Quantity = @Quantity, " +
                                  "CoverImageLocation = @CoverImageLocation, Tags = @Tags where Title = @OldBookTitle",

                    Connection = db
                };

                command.Parameters.AddWithValue("@Title", NewBook.Title);
                command.Parameters.AddWithValue("@Author", NewBook.Author);
                command.Parameters.AddWithValue("@Publisher", NewBook.Publisher);
                command.Parameters.AddWithValue("@ISBN", NewBook.ISBN);
                command.Parameters.AddWithValue("@Quantity", NewBook.Quantity);
                command.Parameters.AddWithValue("@CoverImageLocation", NewBook.CoverImageLocation);
                command.Parameters.AddWithValue("@Tags", NewBook.Tags);
                command.Parameters.AddWithValue("@OldBookTitle", OldBook.Title);

                command.ExecuteNonQuery();
            }

            //GridView expects that the book have only Title, Author, Quantity and CoverImageLocation. There, removing other properties
            OldBook.Publisher = null;
            OldBook.ISBN = null;
            OldBook.Tags = null;

            NewBook.Publisher = null;
            NewBook.ISBN = null;
            NewBook.Tags = null;

            Event_UpdateBook.Invoke(OldBook, NewBook);
        }

        public static ObservableCollection<Book> GetBooks(BookInfoType b)
        {
            ObservableCollection<Book> books = new ObservableCollection<Book>();

            using (SqliteConnection db = new SqliteConnection("Filename=" + DataAccess.BooksFileName))
            {
                db.Open();

                if (b == BookInfoType.Full)
                {
                    SqliteCommand selectCommand = new SqliteCommand
                    {
                        CommandText = "select Title, Author, Publisher, ISBN, Quantity, CoverImageLocation, Tags from MyBooks",
                        Connection = db
                    };

                    SqliteDataReader query = selectCommand.ExecuteReader();

                    while (query.Read())
                    {
                        books.Add(new Book
                        {
                            Title = query.GetString(0),
                            Author = query.GetString(1),
                            Publisher = query.GetString(2),
                            ISBN = query.GetString(3),
                            Quantity = query.GetInt32(4),
                            CoverImageLocation = query.GetString(5),
                            Tags = query.GetString(6)
                        });
                    }
                }

                if (b == BookInfoType.Minimal)
                {
                    SqliteCommand selectCommand = new SqliteCommand
                    {
                        CommandText = "select Title, Author, Quantity, CoverImageLocation from MyBooks",
                        Connection = db
                    };

                    SqliteDataReader query = selectCommand.ExecuteReader();

                    while (query.Read())
                    {
                        books.Add(new Book
                        {
                            Title = query.GetString(0),
                            Author = query.GetString(1),
                            Quantity = query.GetInt32(2),
                            CoverImageLocation = query.GetString(3),
                        });
                    }
                }

                db.Close();
            }

            return books;
        }

        public static Book GetInfoAboutBook(string title)
        {
            using (SqliteConnection db = new SqliteConnection("Filename=" + DataAccess.BooksFileName))
            {
                db.Open();

                Book b = new Book();

                SqliteCommand sqliteCommand = new SqliteCommand
                {
                    Connection = db,
                    CommandText = "select Title, Author, Publisher, ISBN, Quantity, CoverImageLocation, Tags from MyBooks where Title = @Title",
                };

                sqliteCommand.Parameters.AddWithValue("@Title", title);

                SqliteDataReader query = sqliteCommand.ExecuteReader();

                if (query.Read())
                {
                    b.Title = query.GetString(0);
                    b.Author = query.GetString(1);
                    b.Publisher = query.GetString(2);
                    b.ISBN = query.GetString(3);
                    b.Quantity = query.GetInt32(4);
                    b.CoverImageLocation = query.GetString(5);
                    b.Tags = query.GetString(6);
                }

                return b;
            }
        }
    }
    
}