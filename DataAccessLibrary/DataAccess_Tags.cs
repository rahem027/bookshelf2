﻿using Microsoft.Data.Sqlite;
using System.Collections.Generic;

namespace DataAccessLibrary
{
    public static class DataAccess_Tags
    {
        public static void AddTags(string name)
        {
            using (SqliteConnection db = new SqliteConnection("Filename=" + DataAccess.BooksFileName))
            {
                db.Open();

                SqliteCommand command = new SqliteCommand
                {
                    Connection = db,
                    CommandText = "insert into Tags values (@name)"
                };

                command.Parameters.AddWithValue("@name", name);

                command.ExecuteReader();
            }
        }

        public static List<string> GetTags()
        {
            List<string> Tags = new List<string>();

            using (SqliteConnection db = new SqliteConnection("Filename=" + DataAccess.BooksFileName))
            {
                db.Open();

                SqliteCommand command = new SqliteCommand
                {
                    CommandText = "select Name from Tags",
                    Connection = db
                };

                SqliteDataReader query = command.ExecuteReader();

                while (query.Read())
                {
                    Tags.Add(query.GetString(0));
                }
            }

            return Tags;
        }
    }

    
}