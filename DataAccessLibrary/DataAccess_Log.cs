﻿using System;
using Microsoft.Data.Sqlite;

namespace DataAccessLibrary
{
    public class Log
    {
        public string Title { get; set; }
        public string Borrower { get; set; }
        public string Class { get; set; }
        public string Division { get; set; }
        public DateTime DateBorrowed { get; set; }
        public DateTime DateReturned { get; set; }
    }

    public static class DataAccess_Log
    {
        public static void IssueBook(Log log)
        {
            using (SqliteConnection db = new SqliteConnection("Filename=" + DataAccess.LogFileName))
            {
                db.Open();

                SqliteCommand command = new SqliteCommand
                {
                    CommandText = "insert into Log values(@Title, @Borrower, @Class, @Division, @DateBorrowed, null)",
                    Connection = db
                };

                command.Parameters.AddWithValue("@Title", log.Title);
                command.Parameters.AddWithValue("@Borrower", log.Borrower);
                command.Parameters.AddWithValue("@Class", log.Class);
                command.Parameters.AddWithValue("@Division", log.Division);
                command.Parameters.AddWithValue("@DateBorrowed", log.DateBorrowed.Date.ToShortDateString());                

                command.ExecuteReader();
            }
        }

        public static void ReturnBook(Log log)
        {
            using (SqliteConnection db = new SqliteConnection("Filename=" + DataAccess.LogFileName))
            {
                db.Open();
                SqliteCommand command = new SqliteCommand
                {
                    CommandText = "insert into Log (DateReturned) values(@DateReturned) where Title = @Title and Borrower = @Borrower and" +
                    "Class = @Class and Division = @Division and DateBorrowed = @DateBorrowed",

                    Connection = db
                };

                command.Parameters.AddWithValue("@DateReturned", log.DateReturned.ToShortDateString());
                command.Parameters.AddWithValue("@Title", log.Title);
                command.Parameters.AddWithValue("@Borrower", log.Borrower);
                command.Parameters.AddWithValue("@Class", log.Class);
                command.Parameters.AddWithValue("@Division", log.Division);
                command.Parameters.AddWithValue("@DateBorrowed", log.DateBorrowed.ToShortDateString());

                command.ExecuteReader();
            }
        }

        public static void ClearLog()
        {
            using (SqliteConnection db = new SqliteConnection("Filename=" + DataAccess.LogFileName))
            {
                db.Open();

                SqliteCommand command = new SqliteCommand
                {
                    CommandText = "delete from Log"
                };

                command.ExecuteReader();
            }
        }
    }    
}
